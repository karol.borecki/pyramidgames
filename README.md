# PyramidGames



## External assets used in this project:
- Interior (walls, doors etc): [3D Free Modular Kit](https://assetstore.unity.com/packages/3d/environments/3d-free-modular-kit-85732) by Barking Dog
- Chest: [LowPoly Sci-Fi Crates Free](https://assetstore.unity.com/packages/3d/props/lowpoly-sci-fi-crates-free-146016) by AntiDed GameDev
- UI: [Dark Theme UI](https://assetstore.unity.com/packages/2d/gui/dark-theme-ui-199010) by Giniel Villacote
- Key: [Handpainted Keys](https://assetstore.unity.com/packages/3d/handpainted-keys-42044) by RoboCG
- Sounds: [FREE Casual Game SFX Pack](https://assetstore.unity.com/packages/audio/sound-fx/free-casual-game-sfx-pack-54116) by Dustyroom and [Sc-Fi Music](https://assetstore.unity.com/packages/audio/music/sc-fi-music-214312) by Theo Prior Music
