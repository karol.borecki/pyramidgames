using UnityEngine;

namespace ObjectPooler
{
    public interface IPooledObj
    {
        public void OnObjSpawn();
        public void OnObjDisappear();
    }
    
    public abstract class PooledObj : MonoBehaviour, IPooledObj
    {
        public abstract void OnObjSpawn();
        public abstract void OnObjDisappear();
    }
}