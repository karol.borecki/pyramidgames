using System.Collections.Generic;
using UnityEngine;

namespace ObjectPooler
{
    public class ObjectPooler : MonoBehaviour
    {
        [System.Serializable]
        public class Pool
        {
            public string tag;
            public PooledObj prefab;
            public int size;

            public Pool(string tag, PooledObj prefab, int size)
            {
                this.tag = tag;
                this.prefab = prefab;
                this.size = size;
            }
        }
        #region Singleton
        public static ObjectPooler Instance;
        private void Awake()
        {
            Instance = this;
        }
        #endregion
    
        private void Start()
        {
            InitPoolingDic();
        }

        public List<Pool> pools = new List<Pool>();
        private readonly Dictionary<string, Queue<PooledObj>> _poolDictionary = new Dictionary<string, Queue<PooledObj>>();
    
        private void InitPoolingDic()
        {
            foreach (var pool in pools)
                InitPool(pool);
        }

        public PooledObj SpawnFromPool(string poolTag, Vector3 pos, Quaternion rot, Transform parent=null)
        {
            if (!_poolDictionary.ContainsKey(poolTag)) return null;
            var objToSpawn = _poolDictionary[poolTag].Dequeue();

            var objTransform = objToSpawn.transform;
            objTransform.position = pos;
            objTransform.rotation = rot;
            objToSpawn.gameObject.SetActive(true);
        
            var pooledObj = objToSpawn.GetComponent<PooledObj>();
            pooledObj.OnObjSpawn();

            _poolDictionary[poolTag].Enqueue(objToSpawn);
            return objToSpawn;
        }

        public void CreatePool(string poolTag, PooledObj prefab, int size)
        {
            CreatePool(new Pool(poolTag, prefab, size));
        }
    
        public void CreatePool(Pool pool)
        {
            InitPool(pool);
        }

        public void BackToPoolAllObjects(string poolTag)
        {
            var queue = _poolDictionary[poolTag];

            PooledObj queueObj;
            do {
                queueObj = queue.Dequeue();
                if (queueObj == null) break;
                queueObj.gameObject.SetActive(false);
                queue.Enqueue(queueObj);
            } while (queueObj.gameObject.activeSelf) ;


        }

        private void InitPool(Pool pool)
        {
            if (ContainsTag(pool.tag)) return;
            pools.Add(pool);
            var objPool = new Queue<PooledObj>();
        
            for (var i = 0; i < pool.size; i++)
            {
                var obj = Instantiate(pool.prefab).GetComponent<PooledObj>();
                obj.gameObject.SetActive(false);
                objPool.Enqueue(obj);
            }
            _poolDictionary.Add(pool.tag, objPool);
        }

        private bool ContainsTag(string tag)
        {
            return _poolDictionary.ContainsKey(tag);
        }
    }
}
