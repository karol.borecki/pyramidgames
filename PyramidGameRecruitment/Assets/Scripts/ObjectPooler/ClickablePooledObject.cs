using ClickableObjects;

namespace ObjectPooler
{
    public class ClickablePooledObject : PooledObj
    {
        private IPooledObj _resetObj;
        public override void OnObjSpawn()
        {
            _resetObj ??= GetComponent<Clickable>();
            _resetObj?.OnObjSpawn();
        }

        public override void OnObjDisappear()
        {
            _resetObj?.OnObjDisappear();
        }
    }
}
