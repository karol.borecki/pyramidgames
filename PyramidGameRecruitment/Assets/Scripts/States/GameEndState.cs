using Core;
using Gameplay;
using UnityEngine;

namespace States
{
    public class GameEndState : GameState
    {
        private readonly Timer _timer;
        public GameEndState(Timer timer)
        {
            _timer = timer;
        }
        public void OnStateEnter()
        {
            _timer.StopTimer();
        
            UIManager.Instance.ShowGameEndView();
            
            var highScore = LoadDataHandler.GetHighScore();
            var score = _timer.GetTime();
        
            if(score < highScore || highScore <= .0f)
                LoadDataHandler.SaveHighScore(score);
            
            UIManager.Instance.SetGameEndHighScoreText(LoadDataHandler.GetHighScore());
            UIManager.Instance.SetGameEndScoreText(score);
            LoadDataHandler.SaveData();
        }

        public void OnStateExit()
        {
            _timer.ResetTimer();
        }

        public void Tick()
        {
            return;
        }

        public void OnCameraMove(Vector3 dir)
        {
            return;
        }

        public void OnCameraRotate(bool clockwise)
        {
            return;
        }
    
        public void OnActiveClickableMouseClicked(GameObject obj)
        {
            return;
        }

        public void OnClickableMouseOver(GameObject obj)
        {
            return;
        }

        public void OnClickableMouseExit(GameObject obj)
        {
            return;
        }
    }
}
