using UnityEngine;

namespace States
{
    public class PopupState : GameState
    {
        public void OnStateEnter()
        {
            return;
        }

        public void OnStateExit()
        {
            return;
        }

        public void Tick()
        {
            return;
        }

        public void OnCameraMove(Vector3 dir)
        {
            return;
        }

        public void OnCameraRotate(bool clockwise)
        {
            return;
        }

        public void OnActiveClickableMouseClicked(GameObject obj)
        {
            return;
        }

        public void OnClickableMouseOver(GameObject obj)
        {
            return;
        }

        public void OnClickableMouseExit(GameObject obj)
        {
            return;
        }
    }
}
