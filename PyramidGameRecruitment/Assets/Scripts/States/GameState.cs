using UnityEngine;

namespace States
{
    public interface GameState
    {

        public void OnStateEnter();
        public void OnStateExit();
    
        public void Tick();

        public void OnCameraMove(Vector3 dir);
        public void OnCameraRotate(bool clockwise);

        public void OnActiveClickableMouseClicked(GameObject obj);
        public void OnClickableMouseOver(GameObject obj);
        public void OnClickableMouseExit(GameObject obj);
    }
}
