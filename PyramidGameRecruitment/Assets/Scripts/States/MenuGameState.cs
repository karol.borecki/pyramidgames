using CameraControls;
using Core;
using Gameplay;
using UI;
using UnityEngine;

namespace States
{
    internal class MenuGameState : GameState
    {
        private readonly Timer _timer;
    
        private readonly CameraController _camera;
        private readonly Vector3 _cameraStartPos;

        private readonly RoomGenerator _roomGenerator;
        private readonly int _roomSize;
        public MenuGameState(Timer timer, CameraController camera, Vector3 cameraStartPos, RoomGenerator roomGenerator)
        {
            _timer = timer;
        
            _camera = camera;
            _cameraStartPos = cameraStartPos;

            _roomGenerator = roomGenerator;
            _roomSize = _roomGenerator.GetRoomSize();
        }
        public void OnStateEnter()
        {
            UIManager.Instance.ShowMainMenuWindow();
            UIManager.Instance.SetMenuHighScoreText(LoadDataHandler.GetHighScore()); 
            UIManager.Instance.SetKeyImageNotAcquired();
        
            PopupWindowManager.Instance.ClosePopup();
        
            _roomGenerator.CleanScene();

            BackpackManager.Instance.Reset();
        
            _camera.SetupCamera(_cameraStartPos, _roomSize);
        }

        public void OnStateExit()
        {
            _timer.ResetTimer();
            _roomGenerator.Generate();
        }

        public void Tick()
        {
            return;
        }

        public void OnCameraMove(Vector3 dir)
        {
            return;
        }

        public void OnCameraRotate(bool clockwise)
        {
            return;
        }

        public void OnActiveClickableMouseClicked(GameObject obj)
        {
            return;
        }

        public void OnClickableMouseOver(GameObject obj)
        {
            return;
        }

        public void OnClickableMouseExit(GameObject obj)
        {
            return;
        }
    }
}
