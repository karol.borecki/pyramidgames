using CameraControls;
using ClickableObjects;
using Gameplay;
using Sounds;
using UnityEngine;

namespace States
{
    public class PlayState : GameState
    {
        private readonly CameraController _camera;
        private readonly Timer _timer;
        
        public PlayState(CameraController camera, Timer timer)
        {
            _camera = camera;
            _timer = timer;
        }
    
        public void OnStateEnter()
        {
            _timer.StartTimer();
            UIManager.Instance.ShowGameView();
        }

        public void OnStateExit()
        {
            _timer.StopTimer();
        }

        public void Tick()
        {
            UIManager.Instance.SetTimerText(_timer.GetTime());
        }

        public void OnCameraMove(Vector3 dir)
        {
            _camera.Move(dir);
        }

        public void OnCameraRotate(bool clockwise)
        {
            _camera.Rotate(clockwise);
        }
    
        public void OnActiveClickableMouseClicked(GameObject obj)
        {
            var clickable = obj.GetComponent<Clickable>();
            if(clickable == null) return;
            clickable.OnMouseDownHandler();
        }

        public void OnClickableMouseOver(GameObject obj)
        {
            var clickable = obj.GetComponent<Clickable>();
            if(clickable == null) return;
            clickable.OnMouseOverHandler();
        }

        public void OnClickableMouseExit(GameObject obj)
        {
            var clickable = obj.GetComponent<Clickable>();
            if(clickable == null) return;
            clickable.OnMouseOverExitHandler();
        }
    }
}
