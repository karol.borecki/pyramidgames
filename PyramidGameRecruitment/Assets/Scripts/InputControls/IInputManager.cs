using System;
using UnityEngine;

namespace InputControls
{
    public interface IInputManager
    {
        public void AddListenerOnMouseDownEvent(Action<GameObject> listener);
        public void RemoveListenerOnMouseDownEvent(Action<GameObject> listener);
    
        public void AddListenerOnMouseOverEvent(Action<GameObject> listener);
        public void RemoveListenerOnMouseOverEvent(Action<GameObject> listener);
    
        public void AddListenerOnMouseOverExitEvent(Action<GameObject> listener);
        public void RemoveListenerOnMouseOverExitEvent(Action<GameObject> listener);

        public void AddListenerOnCameraRotateEvent(Action<bool> listener);
        public void RemoveListenerOnCameraRotateEvent(Action<bool> listener);
        
        public void AddListenerOnCameraMoveEvent(Action<Vector3> listener);
        public void RemoveListenerOnCameraMoveEvent(Action<Vector3> listener);

    }
}
