using System;
using Config;
using UnityEngine;

namespace InputControls
{
    public class InputManager : MonoBehaviour, IInputManager
    {
        private InputManagerConfig _config;
    
        private Action<GameObject> _onMouseDownHandler;
        private Action<GameObject> _onMouseOverHandler;
        private Action<GameObject> _onMouseOverExitHandler;
        private Action<bool> _onCameraRotateHandler;
        private Action<Vector3> _onCameraMoveEvent;

        private GameObject _gameObjectMouseOver;

        private Camera _mainCamera;

        #region Singleton
        public static InputManager Instance;

        private void Awake()
        {
            Instance = this;

            _mainCamera = Camera.main;
        }
        #endregion

        private void Update()
        {
            HandleMouseDownEvent();
            HandleCameraMoveEvent();
            HandleCameraRotateEvent();
            HandleMouseOverEvent();
        }

        private void HandleMouseOverEvent()
        {
            var obj = GetObjectMouseIsOver();
            if(obj == null)
            {
                if (_gameObjectMouseOver == null) return;
                OnMouseOverExit();
                return;
            }

            if (_gameObjectMouseOver != null) return;
            _onMouseOverHandler?.Invoke(obj);
            _gameObjectMouseOver = obj;
        }

        private void HandleMouseDownEvent()
        {
            if (!Input.GetMouseButtonDown(0) || _gameObjectMouseOver == null) return;
            _onMouseDownHandler?.Invoke(_gameObjectMouseOver);
        }

        private void OnMouseOverExit()
        {
            _onMouseOverExitHandler?.Invoke(_gameObjectMouseOver);
            _gameObjectMouseOver = null;
        }
    
        private void HandleCameraRotateEvent()
        {
            if(Input.GetKey(KeyCode.Q))
                _onCameraRotateHandler?.Invoke(false);   
            if(Input.GetKey(KeyCode.E))
                _onCameraRotateHandler?.Invoke(true);   
        }
    
        private void HandleCameraMoveEvent()
        {
            var movePos = Vector3.zero;

            if (Input.GetKey(KeyCode.W))
                movePos.z = 1;
            if (Input.GetKey(KeyCode.S))
                movePos.z = -1;
            if (Input.GetKey(KeyCode.A))
                movePos.x = -1;
            if (Input.GetKey(KeyCode.D))
                movePos.x = 1;
        
            if(movePos != Vector3.zero) 
                _onCameraMoveEvent?.Invoke(movePos);
        }

        private GameObject GetObjectMouseIsOver()
        {
            var ray = _mainCamera.ScreenPointToRay(Input.mousePosition);
            if (!Physics.Raycast(ray.origin, ray.direction, out var hit, Mathf.Infinity, _config.clickableInputMask)) 
                return null;
            return hit.collider.gameObject;
        }

        public void SetupConfig(InputManagerConfig config)
        {
            _config = config;
        }
    
        #region Observers
        public void AddListenerOnMouseDownEvent(Action<GameObject> listener)
        {
            _onMouseDownHandler += listener;
        }

        public void RemoveListenerOnMouseDownEvent(Action<GameObject> listener)
        {
            _onMouseDownHandler -= listener;
        }
    
        public void AddListenerOnMouseOverEvent(Action<GameObject> listener)
        {
            _onMouseOverHandler += listener;
        }

        public void RemoveListenerOnMouseOverEvent(Action<GameObject> listener)
        {
            _onMouseOverHandler -= listener;
        }
    
        public void AddListenerOnMouseOverExitEvent(Action<GameObject> listener)
        {
            _onMouseOverExitHandler += listener;
        }

        public void RemoveListenerOnMouseOverExitEvent(Action<GameObject> listener)
        {
            _onMouseOverExitHandler -= listener;
        }

        public void AddListenerOnCameraRotateEvent(Action<bool> listener)
        {
            _onCameraRotateHandler += listener;
        }

        public void RemoveListenerOnCameraRotateEvent(Action<bool> listener)
        {
            _onCameraRotateHandler -= listener;
        }

        public void AddListenerOnCameraMoveEvent(Action<Vector3> listener)
        {
            _onCameraMoveEvent += listener;
        }

        public void RemoveListenerOnCameraMoveEvent(Action<Vector3> listener)
        {
            _onCameraMoveEvent -= listener;
        }
        #endregion
    }
}
