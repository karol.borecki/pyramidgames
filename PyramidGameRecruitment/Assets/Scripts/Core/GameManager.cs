using System.Data;
using CameraControls;
using Config;
using Gameplay;
using InputControls;
using Sounds;
using States;
using UI;
using UnityEngine;

namespace Core
{
    public class GameManager : MonoBehaviour
    {
        public CameraController cameraController;
        public Vector3 cameraStartPos;

        public InputManagerConfig inputManagerConfig;
        public SoundManagerConfig soundManagerConfig;
        public UIManagerConfig uiManagerConfig;
        public PopupWindowConfig popupWindowConfig;
        public RoomGeneratorConfig generatorConfig;

        public TimerConfig timerConfig;
        private Timer _timer;

        private GameState _state;
        private MenuGameState _menuState;
        private PlayState _playState;
        private PopupState _popupState;
        private GameEndState _gameEndState;

        private RoomGenerator _roomGenerator;

        #region Singleton

        public static GameManager Instance;

        #endregion
    
        private void Awake()
        {
            Instance = this;
        
            SetupGame();
        
            _menuState = new MenuGameState(_timer, cameraController, cameraStartPos, _roomGenerator);
            _playState = new PlayState(cameraController, _timer);
            _popupState = new PopupState();
            _gameEndState = new GameEndState(_timer);
        
            SwitchToState(_menuState);
        
            UIManager.Instance.AddObserverOnPlayBtnClickedEvent(StartGame);
            UIManager.Instance.AddObserverOnTryAgainBtnClickedEvent(ResetGame);
        
            InputManager.Instance.AddListenerOnCameraMoveEvent(HandleOnCameraMoveEvent);
            InputManager.Instance.AddListenerOnCameraRotateEvent(HandleOnCameraRotateEvent);
            InputManager.Instance.AddListenerOnMouseDownEvent(HandleOnObjectMouseDownEvent);
            InputManager.Instance.AddListenerOnMouseOverEvent(HandleOnObjectMouseOverEvent);
            InputManager.Instance.AddListenerOnMouseOverExitEvent(HandleOnObjectMouseExitEvent);
        }

        private void Update()
        {
            _state.Tick();
        }

        private void SwitchToState(GameState state)
        {
            if(_state == state) return;
            _state?.OnStateExit();
            _state = state;
            _state.OnStateEnter();
        }

        public void StartGame()
        {
            SwitchToState(_playState);
        }

        private void ResetGame()
        {
            SwitchToState(_menuState);
        }
    
        public void EndGame()
        {
            SwitchToState(_gameEndState);
        }

        public void OnPopupShow()
        {
            SwitchToState(_popupState);
        }

        #region InputHandlers
        private void HandleOnObjectMouseDownEvent(GameObject obj)
        {
            _state.OnActiveClickableMouseClicked(obj);
        }
    
        private void HandleOnObjectMouseOverEvent(GameObject obj)
        {
            _state.OnClickableMouseOver(obj);
        }
    
        private void HandleOnObjectMouseExitEvent(GameObject obj)
        {
            _state.OnClickableMouseExit(obj);
        }
    
        private void HandleOnCameraMoveEvent(Vector3 dir)
        {
            _state.OnCameraMove(dir);
        }
    
        private void HandleOnCameraRotateEvent(bool clockwise)
        {
            _state.OnCameraRotate(clockwise);
        }
        #endregion
    
        private void SetupGame()
        {
            if (InputManager.Instance == null)
                new GameObject("Input Manager").AddComponent<InputManager>();
            InputManager.Instance.SetupConfig(inputManagerConfig);
            
            if (SoundManager.Instance == null)
                new GameObject("Sound Manager").AddComponent<SoundManager>();
            SoundManager.Instance.SetupConfig(soundManagerConfig);
        
            if (UIManager.Instance == null)
                new GameObject("UI Manager").AddComponent<UIManager>();
            UIManager.Instance.SetupConfig(uiManagerConfig);
        
            if (PopupWindowManager.Instance == null)
                new GameObject("Popup Window Manager").AddComponent<PopupWindowManager>();
            PopupWindowManager.Instance.SetupConfig(popupWindowConfig);
        
            if (_roomGenerator == null)
                _roomGenerator = new GameObject("Room Generator").AddComponent<RoomGenerator>();
            _roomGenerator.SetupConfig(generatorConfig);
        
            if(BackpackManager.Instance == null)
                new GameObject("Backpack Manager").AddComponent<BackpackManager>();

            if (ObjectPooler.ObjectPooler.Instance == null)
                new GameObject("Object Pooler").AddComponent<ObjectPooler.ObjectPooler>();
            ObjectPooler.ObjectPooler.Instance.CreatePool(generatorConfig.wallPool);
            ObjectPooler.ObjectPooler.Instance.CreatePool(generatorConfig.groundPool);
            ObjectPooler.ObjectPooler.Instance.CreatePool(generatorConfig.doorPool);
            ObjectPooler.ObjectPooler.Instance.CreatePool(generatorConfig.chestPool);

            if (cameraController == null)
                throw new NoNullAllowedException("Camera controller is not defined!");

            if (_timer == null)
                _timer = new GameObject("Timer").AddComponent<Timer>();
            _timer.SetupConfig(timerConfig);
        }
    }
}
