using UnityEngine;

namespace Core
{
    public class LoadDataHandler : MonoBehaviour
    {
        private const string HighScoreVarName = "HighScore";

        public static void SaveHighScore(float score)
        {
            PlayerPrefs.SetFloat(HighScoreVarName, score);
        }

        public static float GetHighScore()
        {
            return PlayerPrefs.GetFloat(HighScoreVarName);
        }

        public static void SaveData()
        {
            PlayerPrefs.Save();
        }
    }
}
