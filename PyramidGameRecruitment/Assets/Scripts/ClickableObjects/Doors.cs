using CameraControls;
using Core;
using Gameplay;
using Sounds;
using UI;
using UnityEngine;

namespace ClickableObjects
{
    public class Doors : Clickable
    {
        public Animator animator;
    
        public PopupInfo doorOpenWithKey;
        public PopupInfo doorOpenWithoutKey;
        
        private CameraEffects _cameraEffects;

        private static readonly int OpenAnimVar = Animator.StringToHash("open");
        private const string DoorOpenSoundTag = "Fanfares";

        public void OnDoorOpenRequest()
        {
            var canOpen = BackpackManager.Instance.isKeyAcquired;
            PopupWindowManager.Instance.ShowPopup(canOpen ? doorOpenWithKey : doorOpenWithoutKey);
            
            if (canOpen) return;
            //That is really bad, but Singleton in this case is worse than that
            _cameraEffects ??= GameManager.Instance.cameraController.GetComponent<CameraEffects>(); 
            _cameraEffects.Shake();
        }

        public void OnDoorOpenResign()
        {
            PopupWindowManager.Instance.ClosePopup();
        }

        public void OnDoorCannotOpenAcceptance()
        {
            PopupWindowManager.Instance.ClosePopup();
        }

        public void OnDoorOpen()
        {
            PopupWindowManager.Instance.ClosePopup();
            SoundManager.Instance.Play(DoorOpenSoundTag);
            Open();
        }
    
        public void OnDoorOpened()
        {
            GameManager.Instance.EndGame();
        }

        private void Open()
        {
            animator.SetBool(OpenAnimVar, true);
        }

        private void Close()
        {
            animator.SetBool(OpenAnimVar, false);
        }

        public override void OnObjSpawn()
        {
            Close();
        }

        public override void OnObjDisappear()
        {
            return;
        }
        
    }
}
