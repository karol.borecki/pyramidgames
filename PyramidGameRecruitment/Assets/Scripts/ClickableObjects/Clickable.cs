using System.Collections.Generic;
using ObjectPooler;
using Sounds;
using UnityEngine;
using UnityEngine.Events;

namespace ClickableObjects
{
    public class Clickable : MonoBehaviour, IPooledObj
    {
        public Color highlightColor;
        public List<MeshRenderer> highlightMeshes;
    
        public UnityEvent onMouseOverEvent;
        public UnityEvent onClickedEvent;

        private bool _isMouseOver;

        private const string ClickableOverSoundTag = "ClickableOver";
        private const string ClickableClickedSoundTag = "ClickableOver";

        private void Start()
        {
            MakeClickable();
        }

        public void OnMouseOverHandler()
        {
            _isMouseOver = true;
            onMouseOverEvent?.Invoke();
            Highlight();
            PlayOverSound();
        }
    
        public void OnMouseOverExitHandler()
        {
            _isMouseOver = false;
            StopHighlight();
        }
    
        public void OnMouseDownHandler()
        {
            if(!_isMouseOver) return;

            onClickedEvent?.Invoke();
            PlayClickedSound();
        }

        private void ChangeMeshesColors(Color color)
        {
            foreach (var mesh in highlightMeshes)
                mesh.material.color = color;
        }

        private void Highlight()
        {
            ChangeMeshesColors(highlightColor);
        }
    
        private void StopHighlight()
        {
            ChangeMeshesColors(Color.white);
        }

        protected void MakeNotClickable()
        {
            StopHighlight();
            gameObject.layer = LayerMask.NameToLayer("Default");
        }

        protected void MakeClickable()
        {
            gameObject.layer = LayerMask.NameToLayer("Clickable");
        }

        public virtual void OnObjSpawn()
        {
            return;
        }

        public virtual void OnObjDisappear()
        {
            return;
        }

        public virtual void PlayClickedSound()
        {
            SoundManager.Instance.Play(ClickableClickedSoundTag);
        }
        
        public virtual void PlayOverSound()
        {
            SoundManager.Instance.Play(ClickableOverSoundTag);
        }
    }
}
