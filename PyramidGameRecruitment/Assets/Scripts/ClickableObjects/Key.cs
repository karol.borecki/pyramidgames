using Core;
using Gameplay;
using Sounds;
using UI;
using UnityEngine;

namespace ClickableObjects
{
    public class Key : Clickable
    {
        public GameObject bodyRef;
        public PopupInfo keyPickupPopup;
        
        private const string KeyPickupSound = "KeyAcquired";

        public void OnKeyPickupRequest()
        {
            PopupWindowManager.Instance.ShowPopup(keyPickupPopup);
        }
    
        public void OnKeyPickup()
        {
            PopupWindowManager.Instance.ClosePopup();
            BackpackManager.Instance.PickupKey();
            SoundManager.Instance.Play(KeyPickupSound);
        
            bodyRef.SetActive(false);
        }

        public void OnKeyPickupCancel()
        {
            PopupWindowManager.Instance.ClosePopup();
        }
    
        public override void OnObjSpawn()
        {
            bodyRef.SetActive(true);
        }

        public override void OnObjDisappear()
        {
        }
    }
}
