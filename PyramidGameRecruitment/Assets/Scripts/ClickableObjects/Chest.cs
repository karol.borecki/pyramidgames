using Sounds;
using UI;
using UnityEngine;

namespace ClickableObjects
{
    public class Chest : Clickable
    {
        public Animator animator;
        public Key key; 
        public PopupInfo chestOpenPopup;

        private static readonly int OpenAnimVar = Animator.StringToHash("open");

        private const string ChestOpenSound = "ChestOpen";

        public void OnChestOpenRequest()
        {
            PopupWindowManager.Instance.ShowPopup(chestOpenPopup);
        }
    
        public void OnOpenCanceled()
        {
            PopupWindowManager.Instance.ClosePopup();
        }

        public void OnChestOpen()
        {
            PopupWindowManager.Instance.ClosePopup();
            SoundManager.Instance.Play(ChestOpenSound);
            Open();
            MakeNotClickable();
        }
    
        private void Open()
        {
            animator.SetBool(OpenAnimVar, true);
        }

        private void Close()
        {
            animator.SetBool(OpenAnimVar, false);
        }

        public override void OnObjSpawn()
        {
            Close();
            MakeClickable();
            key.OnObjSpawn();
        }

        public override void OnObjDisappear()
        {
            return;
        }
    }
}
