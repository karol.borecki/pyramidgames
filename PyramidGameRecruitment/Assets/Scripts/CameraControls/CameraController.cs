using UnityEngine;

namespace CameraControls
{
    public class CameraController : MonoBehaviour
    {
        public float moveSpeed = .1f;
        public float rotateSpeed = .1f;
    
        public float maxDistanceFromWall = 1f;
    
        private int _roomSize;
        private float _cameraYPos;

        private Transform _transform;
        public void SetupCamera(Vector3 pos, int roomSize)
        {
            _transform = gameObject.transform;
            
            _roomSize = roomSize;
            _transform.position = pos;

            _cameraYPos = _transform.position.y;
        }

        public void Move(Vector3 direction)
        {
            _transform.Translate(direction * moveSpeed);
            ClampCameraPos();
        }

        public void Rotate(bool clockwise)
        {
            _transform.Rotate(0, clockwise ? rotateSpeed : -rotateSpeed, 0);
        }

        private void ClampCameraPos()
        {
            var position = _transform.position;
            position = new Vector3(ClampCoordinateToRoomSize(position.x), _cameraYPos, ClampCoordinateToRoomSize(position.z));
            _transform.position = position;
        }

        private float ClampCoordinateToRoomSize(float value)
        {
            return Mathf.Clamp(value, maxDistanceFromWall, _roomSize - maxDistanceFromWall);
        }
    }
}
