using UnityEngine;

namespace CameraControls
{
    public class CameraEffects : MonoBehaviour
    {
        public Animator animator;
        private static readonly int ShakeAnimVar = Animator.StringToHash("shake");

        public void Shake()
        {
            animator.SetTrigger(ShakeAnimVar);
        }
    }
}
