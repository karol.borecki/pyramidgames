using System;
using Config;
using Sounds;
using UI;
using UnityEngine;
using UnityEngine.Events;

public class UIManager : MonoBehaviour
{
    private UIManagerConfig _config;

    private const string BtnClickSoundTag = "UIClicked";
    
    #region Singleton
    public static UIManager Instance;

    private void Awake()
    {
        Instance = this;
    }
    #endregion
    
    private void Start()
    {
        SetupBtnClickSounds();
    }

    public void ShowMainMenuWindow()
    {
        _config.gameViewPanel.SetActive(false);
        _config.gameOverPanel.SetActive(false);
        _config.mainMenuPanel.SetActive(true);

        PopupWindowManager.Instance.ClosePopup();
    }

    public void ShowGameView()
    {
        _config.mainMenuPanel.SetActive(false);
        _config.gameOverPanel.SetActive(false);
        _config.gameViewPanel.SetActive(true);
        PopupWindowManager.Instance.ClosePopup();
    }

    public void ShowGameEndView()
    {
        _config.mainMenuPanel.SetActive(false);
        _config.gameViewPanel.SetActive(false);
        _config.gameOverPanel.SetActive(true);
        
        PopupWindowManager.Instance.ClosePopup();
    }

    public void SetKeyImageAcquired()
    {
        _config.keyImage.sprite = _config.keySprite;
    }
    
    public void SetKeyImageNotAcquired()
    {
        _config.keyImage.sprite = _config.blankSprite;
    }

    public void SetMenuHighScoreText(float score)
    {
        _config.menuHighScoreText.text = score.ToString("F1") + "s";
    }
    
    public void SetGameEndHighScoreText(float score)
    {
        _config.gameEndHighScoreText.text = score.ToString("F1");
    }
    
    public void SetGameEndScoreText(float score)
    {
        _config.gameEndScoreText.text = score.ToString("F1");
    }
    
    public void SetTimerText(float time)
    {
        _config.timerText.text = time.ToString("F1");
    }
    
    public void SetupConfig(UIManagerConfig config)
    {
        _config = config;
    }

    private void SetupBtnClickSounds()
    {
        _config.playBtn.onClick.AddListener(PlayBtnClickSound);
        _config.tryAgainBtn.onClick.AddListener(PlayBtnClickSound);
    }

    public static void PlayBtnClickSound()
    {
        SoundManager.Instance.Play(BtnClickSoundTag);
    }
    
    #region Observers
    public void AddObserverOnPlayBtnClickedEvent(UnityAction l)
    {
        _config.playBtn.onClick.AddListener(l);
    }
    
    public void RemoveObserverOnPlayBtnClickedEvent(UnityAction l)
    {
        _config.playBtn.onClick.RemoveListener(l);
    }
    
    public void AddObserverOnTryAgainBtnClickedEvent(UnityAction l)
    {
        _config.tryAgainBtn.onClick.AddListener(l);
    }
    
    public void RemoveObserverOnTryAgainBtnClickedEvent(UnityAction l)
    {
        _config.tryAgainBtn.onClick.RemoveListener(l);
    }
    #endregion
}
