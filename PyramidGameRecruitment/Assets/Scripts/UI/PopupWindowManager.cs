using System;
using System.Collections.Generic;
using Config;
using Core;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UI
{
    [Serializable]
    public struct PopupChoiceEvent
    {
        public string btnText;
        public UnityEvent choiceEvent;
    }

    [Serializable]
    public struct PopupInfo
    {
        public string txtBody;
        public List<PopupChoiceEvent> choicesEvents;
    }

    public class PopupWindowManager : MonoBehaviour
    {
        private PopupWindowConfig _config;
        private bool IsPopupShown { get; set; }

        #region Singleton
        public static PopupWindowManager Instance;
        private void Awake()
        {
            Instance = this;
        }

        #endregion

        public void ShowPopup(PopupInfo info)
        {
            if(IsPopupShown) return;
            GameManager.Instance.OnPopupShow();
            _config.popupText.text = info.txtBody;
            foreach (var choiceEvent in info.choicesEvents)
                CreateChoiceBtn(choiceEvent);
            _config.popupWindow.SetActive(true);
            IsPopupShown = true;
        }

        public void ClosePopup()
        {
            if(!IsPopupShown) return;
            GameManager.Instance.StartGame();
            _config.popupText.text = "";
            foreach (Transform choiceEventBtn in _config.popupChoicePanel.transform)
                Destroy(choiceEventBtn.gameObject);
            _config.popupWindow.SetActive(false);
            IsPopupShown = false;
        }

        private void CreateChoiceBtn(PopupChoiceEvent choiceEvent)
        {
            var btn = Instantiate(_config.btnPrefab, _config.popupChoicePanel.transform, true);
            btn.onClick.AddListener(delegate { choiceEvent.choiceEvent?.Invoke(); });
            btn.onClick.AddListener(UIManager.PlayBtnClickSound);
            btn.GetComponentInChildren<Text>().text = choiceEvent.btnText;
        }

        public void SetupConfig(PopupWindowConfig config)
        {
            _config = config;
        }
    }
}