using System;
using System.Collections.Generic;
using System.Linq;
using Config;
using UnityEngine;

namespace Sounds
{
    [Serializable]
    public class Sound
    {
        public string tag;
        public AudioClip clip;
        private AudioSource _source;
    
        [Range(0, 1f)]
        public float volume;

        public void Setup(AudioSource source)
        {
            _source = source;
        
            source.clip = clip;
            source.volume = volume;
        }

        public void Play()
        {
            _source.Play();
        }
    }

    public class SoundManager : MonoBehaviour
    {
        private SoundManagerConfig _config;
        
        #region Singleton

        public static SoundManager Instance;
        private void Awake()
        {
            Instance = this;
        }

        #endregion
        
        private void Start()
        {
            foreach (var sound in _config.audioClips)
                sound.Setup(gameObject.AddComponent<AudioSource>());
            
            _config.mainTheme.Setup(gameObject.AddComponent<AudioSource>());
            _config.mainTheme.Play();
        }

        public void Play(string soundTag)
        {
            var sound = _config.audioClips.First(s => s.tag.Equals(soundTag));
            sound.Play();
        }

        public void SetupConfig(SoundManagerConfig config)
        {
            _config = config;
        }
    }
}