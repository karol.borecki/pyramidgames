using System;
using System.Collections.Generic;
using Sounds;
using UnityEngine;
using UnityEngine.UI;

namespace Config
{
    [Serializable]
    public struct InputManagerConfig
    {
        public LayerMask clickableInputMask;
    }

    [Serializable]
    public struct UIManagerConfig
    {
        public GameObject mainMenuPanel;
        public GameObject gameViewPanel;
        public GameObject gameOverPanel;
    
        public Button playBtn;
        public Button tryAgainBtn;

        public Image keyImage;

        public Sprite blankSprite;
        public Sprite keySprite;
    
        public Text menuHighScoreText;
        public Text gameEndHighScoreText;
        public Text gameEndScoreText;
        public Text timerText;
    }

    [Serializable]
    public struct PopupWindowConfig
    {
        public GameObject popupWindow;
        public Text popupText;
        public GameObject popupChoicePanel;
    
        public Button btnPrefab; //TODO Jak ja mam kurwa dostać się do txt btn?
    }

    [Serializable]
    public struct RoomGeneratorConfig
    {
        public int cellSize;
        public int roomSize;

        public float roomY;

        public float minObjectsDistanceFromWall;

        public ObjectPooler.ObjectPooler.Pool wallPool;
        public ObjectPooler.ObjectPooler.Pool groundPool;
        public ObjectPooler.ObjectPooler.Pool doorPool;
        public ObjectPooler.ObjectPooler.Pool chestPool;
    }

    [Serializable]
    public struct SoundManagerConfig
    {
        public Sound mainTheme;
        public List<Sound> audioClips;
    }

    [Serializable]
    public struct TimerConfig
    {
        public float refreshRate;
    }
}