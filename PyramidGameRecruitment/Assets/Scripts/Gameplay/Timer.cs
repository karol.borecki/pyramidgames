using System.Collections;
using Config;
using UnityEngine;

namespace Gameplay
{
    public class Timer : MonoBehaviour
    {
        private TimerConfig _config;
    
        private float _time;
        private bool _isCounting;
    
        private IEnumerator _countCoroutine;
    
        public float GetTime() => _time;
        public bool IsCounting() => _isCounting;

        public void StartTimer()
        {
            if(_isCounting) return;
        
            StartCoroutine(_countCoroutine);
            _isCounting = true;
        }

        public void ResetTimer()
        {
            _time = 0;
        }

        public void StopTimer()
        {
            if(!_isCounting) return;
        
            StartCoroutine(_countCoroutine);
            _isCounting = false;
        }

        public void StopAndReset()
        {
            StopTimer();
            ResetTimer();
        }
    
        public void ResetAndStart()
        {
            ResetTimer();
            StartTimer();
        }

        private IEnumerator CountTime()
        {
            var wait = new WaitForSeconds(_config.refreshRate);
            while (true)
            {
                yield return wait;
                _time += _config.refreshRate;
            }
        }

        public void SetupConfig(TimerConfig config)
        {
            _config = config;
            _countCoroutine = CountTime();
        }
    }
}
