using Config;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Gameplay
{
    public class RoomGenerator : MonoBehaviour
    {
        private RoomGeneratorConfig _config;

        public void Generate()
        {
            GeneratePlane(_config.roomY);
            GenerateWalls();
            GenerateChest();
        }

        private void GeneratePlane(float y)
        {
            for(var x=0; x<_config.roomSize; x+=_config.cellSize)
            for (var z = 0; z < _config.roomSize; z+=_config.cellSize)
                InstantiatePlane(new Vector3(x, y, z));
        }

        private void GenerateWalls()
        {
            var wallYRotation = 0;
            var doorWallIndex = GetRandomWallIndex();
            var doorWallCoordinate = GetRandomWallCoordinate();

            for (var wallIndex = 0; wallIndex < 4; wallIndex++)
            {
                for (var wallCoordinate = 0; wallCoordinate < _config.roomSize; wallCoordinate += _config.cellSize)
                {
                    var placeDoors = wallIndex == doorWallIndex && wallCoordinate == doorWallCoordinate;
                    if (wallIndex == 0) InstantiateWall(wallCoordinate, 0, wallYRotation, placeDoors);
                    else if (wallIndex == 1) InstantiateWall(_config.roomSize, wallCoordinate, wallYRotation, placeDoors);
                    else if (wallIndex == 2) InstantiateWall(_config.roomSize - wallCoordinate, _config.roomSize, wallYRotation, placeDoors);
                    else if (wallIndex == 3) InstantiateWall(0, _config.roomSize - wallCoordinate, wallYRotation, placeDoors);
                }

                wallYRotation -= 90;
            }
        }
    
        private void GenerateChest()
        {
            InstantiateChest();
        }

        public void CleanScene()
        {
            ObjectPooler.ObjectPooler.Instance.BackToPoolAllObjects(_config.wallPool.tag);
            ObjectPooler.ObjectPooler.Instance.BackToPoolAllObjects(_config.groundPool.tag);
            ObjectPooler.ObjectPooler.Instance.BackToPoolAllObjects(_config.chestPool.tag);
            ObjectPooler.ObjectPooler.Instance.BackToPoolAllObjects(_config.doorPool.tag);
        }

        private void InstantiateWall(float x, float z, float wallYRotation, bool placeDoors=false)
        {
            ObjectPooler.ObjectPooler.Instance.SpawnFromPool(placeDoors ? _config.doorPool.tag : _config.wallPool.tag,
                new Vector3(x, _config.roomY, z), Quaternion.Euler(0, wallYRotation, 0));
        }

        private void InstantiatePlane(Vector3 pos)
        {
            ObjectPooler.ObjectPooler.Instance.SpawnFromPool(_config.groundPool.tag, pos, Quaternion.identity);
        }

        private void InstantiateChest()
        {
            ObjectPooler.ObjectPooler.Instance.SpawnFromPool(_config.chestPool.tag, GetRandomRoomObjectPos(), 
                Quaternion.Euler(0, Random.Range(0, 180), 0));
        }

        public int GetRoomSize() => _config.roomSize;

        #region GetterOfRandomValues
        private static int GetRandomWallIndex() => Random.Range(0, 3);
        private int GetRandomWallCoordinate() => Random.Range(0, _config.roomSize/_config.cellSize) * _config.cellSize;
        private Vector3 GetRandomRoomObjectPos()=> new Vector3(GetRandomRoomCoordinate(), _config.roomY, GetRandomRoomCoordinate());
        private float GetRandomRoomCoordinate() => Random.Range(_config.minObjectsDistanceFromWall, _config.roomSize-_config.minObjectsDistanceFromWall);
        #endregion
        
        public void SetupConfig(RoomGeneratorConfig config)
        {
            _config = config;
        }
    }
}
