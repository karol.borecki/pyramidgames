using UnityEngine;

namespace Gameplay
{
    public class BackpackManager : MonoBehaviour
    {
        public bool isKeyAcquired;
    
        #region Singleton

        public static BackpackManager Instance;

        private void Awake()
        {
            Instance = this;
        }

        #endregion

        public void PickupKey()
        {
            isKeyAcquired = true;
            UIManager.Instance.SetKeyImageAcquired();
        }

        public void Reset()
        {
            isKeyAcquired = false;
            UIManager.Instance.SetKeyImageNotAcquired();
        }
    }
}
